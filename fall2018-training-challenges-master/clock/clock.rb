class Clock

class << self
	alias :at :new
end


attr_accessor :hours
attr_accessor :minutes


def initialize(hours = 0, minutes = 0)
	@hours = (hours + (minutes / 60)) % 24
	@minutes = (minutes % 60)
end

def to_s
	'%02i:%02i' % [*time]
end

def time
	[@hours, @minutes]
end

def +(minutes)
	Clock.at(hours, @minutes + minutes)
end

def ==(other)
	to_s == other.to_s
end


end

module BookKeeping

VERSION = 2

end
