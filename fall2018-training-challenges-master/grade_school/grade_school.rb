module BookKeeping
	VERSION = 3
end

class School

	attr_accessor :roster

	def initialize
		@roster = Hash.new { [] }
	end

#alias :students :[]

def students(grade)
	roster[grade].sort
end

	def add(student, grade)
		roster[grade] <<= student
	end

	def students_by_grade
		@roster.keys.sort.map do |grade|
			{
				grade: grade,
				students: students(grade)
			}
		end
	end


end

#school = School.new

