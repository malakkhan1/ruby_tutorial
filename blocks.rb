def sandwich
	puts "top bread"
	yield
	puts "botton bread"
end

sandwich do
	puts "mutton, lettuce, and tomato"
end

def tag(tagname, text)
	html = "<#{tagname}>#{text}</#{tagname}>"
	yield html
end

tag("p", "etc") do |markup|
	puts markup
end

